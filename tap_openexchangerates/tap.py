"""openexchangerates tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_openexchangerates.streams import CurrencyStream

STREAM_TYPES = [CurrencyStream]


class Tapopenexchangerates(Tap):
    """openexchangerates tap class."""

    name = "tap-openexchangerates"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "base", th.StringType, required=True, description="The base currency"
        ),
        th.Property("symbols", th.StringType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapopenexchangerates.cli()
