"""Stream type classes for tap-openexchangerates."""

from singer_sdk import typing as th

from tap_openexchangerates.client import openexchangeratesStream


class CurrencyStream(openexchangeratesStream):
    """Define custom stream."""

    name = "rates"
    path = "time-series.json"
    primary_keys = ["rates"]
    replication_key = "date"
    schema = th.PropertiesList(
        th.Property("date", th.DateType),
        th.Property("base", th.StringType),
        th.Property("rates", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()
