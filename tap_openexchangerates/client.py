"""REST client handling, including openexchangeratesStream base class."""

from datetime import datetime, timedelta
from typing import Any, Dict, Iterable, List, Optional

import requests
from memoization import cached
from pendulum import parse
from singer_sdk.streams import RESTStream


class openexchangeratesStream(RESTStream):
    """openexchangerates stream class."""

    url_base = "https://openexchangerates.org/api/"

    records_jsonpath = "$.[*]"
    init_date = None

    @property
    def partitions(self) -> Optional[List[dict]]:
        symbols = self.extract_symbols(self.config["symbols"])
        symbols = ",".join(symbols)
        return [dict(symbols=symbols, base=self.config["base"].upper())]

    @staticmethod
    def extract_symbols(symbols):
        symbols = [s.strip().upper() for s in symbols.split(",")]
        return sorted(symbols)

    @property
    def symbols(self):
        return self.extract_symbols(self.config["symbols"])

    @cached
    def symbols_states(self):
        start_date = parse(self.config.get("start_date", "2000-01-01")).replace(
            tzinfo=None
        )
        states = {p: start_date for p in self.symbols}
        partition_states = self.stream_state.get("partitions", [])
        for symbol in states:
            for partition in partition_states:
                symbols = partition.get("context", {}).get("symbols", "")
                base = partition.get("context", {}).get("base", "").upper()
                config_base = self.config["base"].upper()
                if base != config_base:
                    continue
                symbols = self.extract_symbols(symbols)
                if symbol in symbols:
                    new_state = partition.get("replication_key_value")
                    if new_state:
                        new_state = parse(new_state).replace(tzinfo=None)
                        if new_state > states[symbol]:
                            states[symbol] = new_state
        return states

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        today = datetime.utcnow().date()

        json_response = response.json()
        end_date = datetime.strptime(json_response["end_date"], "%Y-%m-%d").date()

        if end_date >= today:
            return None

        return end_date + timedelta(1)

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        today = datetime.utcnow().date()

        params: dict = {}
        if not self.init_date:
            start_date = min(self.symbols_states().values())
            self.init_date = start_date.date()

        params["app_id"] = self.config.get("access_key")
        params["base"] = self.config.get("base")
        start_date = next_page_token or self.init_date
        end_date = start_date + timedelta(30)

        if end_date >= today:
            end_date = today
        if start_date > today:
            start_date = today

        params["start"] = start_date.strftime("%Y-%m-%d")
        params["end"] = end_date.strftime("%Y-%m-%d")
        params["symbols"] = self.config.get("symbols")

        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        rates = response.json().get("rates")
        for date, rate in rates.items():
            yield dict(date=date, base=self.config.get("base"), rates=rate)

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        symbol_state = self.symbols_states()
        rates_dict = {}
        row["date"] = parse(row["date"]).replace(tzinfo=None)
        for symbol, value in row["rates"].items():
            if symbol in self.symbols and value:
                if symbol_state[symbol] < row["date"]:
                    rates_dict[symbol] = value
        if rates_dict:
            row["rates"] = rates_dict
            row["date"] = row["date"].strftime("%Y-%m-%d")
            return row
